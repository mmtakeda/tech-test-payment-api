﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentAPI.Interface
{
    using DTO;

    public interface IVendas
    {
        public VendaDTO Registrar(VendaDTO pedido);
        public IEnumerable<VendaDTO> Buscar();
        public VendaDTO Buscar(int idVenda);
        public IEnumerable<ProdutoDTO> ListarProdutos();
        public string AtualizarStatus(int idVenda, string statusVenda);
    }
}
