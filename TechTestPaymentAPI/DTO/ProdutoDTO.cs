﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentAPI.DTO
{
    public class ProdutoDTO
    {
        public string Id { get; set; }
        public string Descricao { get; set; }
        public double ValorUnitario { get; set; }
    }
}
