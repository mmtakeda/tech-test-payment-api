﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentAPI.DTO
{
    public static class StatusDTO
    {
        public const string C_NovaVenda = "Nova venda";
        public const string C_AguardandoPagamento = "Aguardando Pagamento";
        public const string C_PagamentoAprovado = "Pagamento Aprovado";
        public const string C_EnviadoParaTransportador = "Enviado para Transportadora";
        public const string C_Entregue = "Entregue";
        public const string C_Cancelada = "Cancelada";
        public const string C_VendaVazio = "[ERRO]:: Venda sem produtos";
        public const string C_VendaJaRegistrado = "[ERRO]:: Venda ja registrado";
    }
}
