﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentAPI.DTO
{
    public class ItemVendaDTO
    {
        private double _valorItem = 0;
        private int _qtde = 0;

        public int Id { get; set; }

        public ProdutoDTO Produto { get; set; }

        public int Quantidade
        {
            get { return _qtde; }
            set { _qtde = value;

                if (Produto != null) _valorItem = _qtde * Produto.ValorUnitario;
            }
        }

        public double Valor { get { return _valorItem; } }
    }
}
