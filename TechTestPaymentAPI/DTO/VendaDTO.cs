﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentAPI.DTO
{
    public class VendaDTO
    {
        private double _valorTotal = 0;
        private IEnumerable<ItemVendaDTO> _itens;

        [Required]
        public int Id { get; set; }
        [Required]
        public DateTime DtEmissao { get; set; }
        [Required]
        public VendedorDTO Vendedor { get; set; }
        [Required]
        public IEnumerable<ItemVendaDTO> Itens {
            get { return _itens; }
            set
            {
                if (value != null)
                {
                    _itens = value.ToList();
                    CalculaTotal();
                }
            }
        }
        public string Status { get; set; }
        public double ValorTotal { get { return _valorTotal; } }
        public string Mensagem { get; set; }
        private double CalculaTotal()
        {
            _valorTotal = Itens.Sum(a => a.Valor);
            return _valorTotal;
        }
    }
}
