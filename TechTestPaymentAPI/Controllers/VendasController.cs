﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechTestPaymentAPI.Controllers
{
    using DTO;
    using Interface;

    [ApiConventionType(typeof(DefaultApiConventions))]
    [Route("api/[controller]")]
    [ApiController]
    public class VendasController : ControllerBase
    {
        private readonly IVendas _vendasService;

        public VendasController(IVendas vendasService)
        {
            _vendasService = vendasService;
        }

        [HttpPost]
        public ActionResult Registrar(VendaDTO venda)
        {
            try
            {
                if (venda == null) return BadRequest();

                //Retorna ERRO 400 caso a venda nao tenha itens no carrinho
                if (venda.Itens.Count() < 1)
                {
                    venda.Status = StatusDTO.C_VendaVazio;
                    venda.Mensagem = "Venda sem produtos inserido no carrinho.";
                    return BadRequest(venda);
                }

                //registra a venda
                venda = _vendasService.Registrar(venda);

                //Retorna ERRO 400, caso o status seja diferente de aguardando pagamento
                if (venda.Status != StatusDTO.C_AguardandoPagamento) return BadRequest(venda);

                //Retorna HTTP:201 para venda registrada com sucesso.
                return CreatedAtAction(actionName: "Registrar", venda);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        public ActionResult<IEnumerable<VendaDTO>> Buscar()
        {
            try
            {
                return Ok(_vendasService.Buscar());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("produtos")]
        public ActionResult<IEnumerable<ProdutoDTO>> ListarProdutos()
        {
            try
            {
                return Ok(_vendasService.ListarProdutos());
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("{idVenda:int}")]
        public ActionResult<VendaDTO> BuscarPorId(int idVenda)
        {
            try
            {
                if (idVenda < 0)
                    return BadRequest();

                var _venda = _vendasService.Buscar(idVenda);
                if (_venda != null) return Ok(_venda);

                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("{idVenda:int}")]
        public ActionResult AtualizarStatus(int idVenda, string statusVenda)
        {
            try
            {
                var result = _vendasService.AtualizarStatus(idVenda, statusVenda);
                if (!result.Equals(string.Empty)) return BadRequest(result);

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
