﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;

namespace TechTestPaymentAPI.Domain
{
    using DTO;
    using Interface;

    public class VendasDomain : IVendas
    {
        private List<VendaDTO> _listaVendasRealizadas;
        private List<ProdutoDTO> _listaProdutos;
        private IMemoryCache _memoryVendas;
        private const string C_VENDAS_KEY = "VENDAS";

        public VendasDomain(IMemoryCache memory)
        {
            _memoryVendas = memory;

            CriaProdutos();
            CriarUmaVenda();
        }

        string IVendas.AtualizarStatus(int idVenda, string statusVenda)
        {
            var _validado = ValidarStatusPedido(idVenda, statusVenda);
            if (!_validado.Equals(string.Empty)) return _validado;

            var _indice = _listaVendasRealizadas.FindIndex(ven => ven.Id == idVenda);
            string _msg = String.Format("Status alterado DE: {0} - PARA: {1} em {2}", _listaVendasRealizadas[_indice].Status, statusVenda, DateTime.Now);

            _listaVendasRealizadas[_indice].Status = statusVenda;
            _listaVendasRealizadas[_indice].Mensagem = _msg;

            return string.Empty;
        }

        IEnumerable<ProdutoDTO> IVendas.ListarProdutos()
        {
            return _listaProdutos;
        }

        IEnumerable<VendaDTO> IVendas.Buscar()
        {
            return _listaVendasRealizadas;
        }

        VendaDTO IVendas.Buscar(int idVenda)
        {
            return _listaVendasRealizadas.Find(ve => ve.Id == idVenda); ;
        }

        VendaDTO IVendas.Registrar(VendaDTO venda)
        {
            if (_listaVendasRealizadas.Exists(ve => ve.Id == venda.Id))
            {
                venda.Status = StatusDTO.C_VendaJaRegistrado;
                venda.Mensagem = "Venda ja foi registrado";
                return venda;
            }

            //Seta status da venda
            venda.Status = StatusDTO.C_AguardandoPagamento;
            venda.Mensagem = "Venda confirmado, aguardando pagamento.";
            _listaVendasRealizadas.Add(venda);

            AdicionaVendasEmMemoria();

            return venda;
        }

        private void CriarUmaVenda()
        {
            //Valida se lista de vendas ja existe em memoria
            if (_memoryVendas.TryGetValue(C_VENDAS_KEY, out _listaVendasRealizadas)) return;

            //Instancia lista caso nao tenha sido inicializado
            if (_listaVendasRealizadas == null)  _listaVendasRealizadas = new List<VendaDTO>();

            //Cria objetos para criar uma venda inicial e armazenar na memoria
            VendedorDTO _novoVendedor = new VendedorDTO() { Id = 99, Nome = "Paulo", Cpf = "123.456.789-01", Email = "paulo@teste.com", Telefone = "(11)99876-5432" };
            List<ItemVendaDTO> _itens = new List<ItemVendaDTO>();
            _itens.Add(new ItemVendaDTO() { Id = 1, Produto = _listaProdutos.Find( prod => prod.Id == "MONITOR001"), Quantidade = 2});
            VendaDTO _novoPedido = new VendaDTO() { Id = 1, 
                                                        DtEmissao = DateTime.Now, 
                                                        Status = StatusDTO.C_AguardandoPagamento, 
                                                        Mensagem = "Venda confirmado, aguardando pagamento.",
                                                        Vendedor = _novoVendedor,
                                                        Itens = _itens};

            _listaVendasRealizadas.Add(_novoPedido);

            AdicionaVendasEmMemoria();
        }

        private void CriaProdutos()
        {
            if (_listaProdutos == null)
                _listaProdutos = new List<ProdutoDTO>();

            _listaProdutos.Add(new ProdutoDTO() { Id = "MONITOR001", Descricao = "Monitor 21", ValorUnitario = 1500.00 });
            _listaProdutos.Add(new ProdutoDTO() { Id = "TECLADO001", Descricao = "Teclado ABNT2", ValorUnitario = 70.50 });
            _listaProdutos.Add(new ProdutoDTO() { Id = "MONITOR002", Descricao = "Monitor 15", ValorUnitario = 958.50 });
            _listaProdutos.Add(new ProdutoDTO() { Id = "CADEIRA001", Descricao = "Cadeira Gamer", ValorUnitario = 3450.00 });
        }

        private void AdicionaVendasEmMemoria()
        {
            var memoryCaheEntrOptios = new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5),
                SlidingExpiration = TimeSpan.FromMinutes(2)

            };
            _memoryVendas.Set(C_VENDAS_KEY, _listaVendasRealizadas, memoryCaheEntrOptios);
        }

        private string ValidarStatusPedido(int idVendaAtualizar, string statusVendaAtualizar)
        {
            if (!statusVendaAtualizar.Equals(StatusDTO.C_AguardandoPagamento) && !statusVendaAtualizar.Equals(StatusDTO.C_Cancelada) &&
                !statusVendaAtualizar.Equals(StatusDTO.C_Entregue) && !statusVendaAtualizar.Equals(StatusDTO.C_EnviadoParaTransportador) &&
                !statusVendaAtualizar.Equals(StatusDTO.C_PagamentoAprovado))
                return "ERRO:: Status de venda invalido";

            //Retornar erro se ID da venda nao estiver registrado
            if (!_listaVendasRealizadas.Exists(ven => ven.Id == idVendaAtualizar)) return String.Format("ERRO:: Venda ID:{0} nao registrado.", idVendaAtualizar);

            //Valida condicoes para mudanca de status
            var _venda = _listaVendasRealizadas.First(ven => ven.Id == idVendaAtualizar);
            if (statusVendaAtualizar.Equals(StatusDTO.C_PagamentoAprovado) && !_venda.Status.Equals(StatusDTO.C_AguardandoPagamento)) return String.Format("ERRO:: Alteracao de status DE: {0} - PARA: {1} eh invalida para a Venda ID: {2}.", _venda.Status, statusVendaAtualizar, idVendaAtualizar);

            if (statusVendaAtualizar.Equals(StatusDTO.C_Cancelada) && (!_venda.Status.Equals(StatusDTO.C_AguardandoPagamento) || !_venda.Status.Equals(StatusDTO.C_PagamentoAprovado))) return String.Format("ERRO:: Alteracao de status DE: {0} - PARA: {1} eh invalida para a Venda ID: {2}.", _venda.Status, statusVendaAtualizar, idVendaAtualizar);

            if (statusVendaAtualizar.Equals(StatusDTO.C_EnviadoParaTransportador) && !_venda.Status.Equals(StatusDTO.C_PagamentoAprovado)) return String.Format("ERRO:: Alteracao de status DE: {0} - PARA: {1} eh invalida  para a Venda ID: {2}.", _venda.Status, statusVendaAtualizar, idVendaAtualizar);

            if (statusVendaAtualizar.Equals(StatusDTO.C_Entregue) && !_venda.Status.Equals(StatusDTO.C_EnviadoParaTransportador)) return String.Format("ERRO:: Alteracao de status DE: {0} - PARA: {1} eh invalida  para a Venda ID: {2}.", _venda.Status, statusVendaAtualizar, idVendaAtualizar);

            return "";
        }
    }
}
