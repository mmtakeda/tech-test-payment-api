using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestPaymentAPI
{
    using TechTestPaymentAPI.Controllers;
    using TechTestPaymentAPI.Interface;

    [TestClass]
    public class TestRegistrarVenda
    {
        [TestMethod]
        public void RegistrarVenda_VendaNovo_AdicionaVenda()
        {
            //Arrange  
            IVendas _vendas = new VendasFake();
            VendasController _controller = new VendasController(_vendas);

            //Act
            var _result = _controller.Registrar(null);

            //Assert
            Assert.IsTrue(true == true);
        }

        [TestMethod]
        public void RegistrarVenda_VendaJaExiste_Retorna200StatusVendaJaExiste()
        {
            //Arrange  
            IVendas _vendas = new VendasFake();
            VendasController _controller = new VendasController(_vendas);

            //Act
            var _result = _controller.Registrar(null);

            //Assert
            Assert.IsTrue(true == true);
        }

        [TestMethod]
        public void RegistrarVenda_VendaSemItens_Retorna200StatusVendaSemItens()
        {
            //Arrange  
            IVendas _vendas = new VendasFake();
            VendasController _controller = new VendasController(_vendas);

            //Act
            var _result = _controller.Registrar(null);

            //Assert
            Assert.IsTrue(true == true);
        }
    }
}
